from django.shortcuts import render
from django.http import HttpResponse
from .models import Article

# Create your views here.


def index(request):
    # return HttpResponse('foobar')
    query = request.GET['query']
    start_date = request.GET['start']
    end_date = request.GET['end']
    articles = Article.objects.filter(text__icontains=query)
    def mapper(a):
        return {
            'title': a.title,
            'split_text': a.text.split('\n'),
            'source': (a.source or a.link),
            'link': a.link,
        }
    return render(request, 'search.html', {
        'article_list': map(mapper, articles),
        'article_list_length':len(articles),
    })

def new_article(request):
    if request.method == 'POST':
        print ('Raw Data: "%s"' % request.body)
        if request.is_ajax():
            return HttpResponse("OK")
    return render(request, 'new.html')

def test_haml(request):
    query='samsung'
    articles = Article.objects.filter(text__icontains=query)
    def mapper(a):
        return {
            'title': a.title,
            'split_text': a.text.split('\n'),
            'source': (a.source or a.link),
            'link': a.link,
        }
    return render(request, 'search_e.haml', {'article_list': map(mapper, articles)})