# coding=utf-8
import bs4
import re

# from articles.models import Article


def get_articles(page):
    soup = bs4.BeautifulSoup(page, 'html5lib')
    hs = soup.findAll('h2')
    articles = []
    for h in hs:
        e = h.nextSibling
        a = {'title': h.text}
        ps = []
        links = []
        ts = []
        while e.name != 'hr':
            if not e.name:
                ts.append(str(e))
            if e.name and e.name != 'br':
                ps.append(e.text)
            if e.name == 'a':
                links.append(e.text)
            e = e.nextSibling
        a['text'] = '\n'.join(ps)
        # a['links']=' '.join(links)
        hlinks = list(filter(lambda x: x.startswith('http'), links))
        if hlinks:
            a['link'] = hlinks[0]
        # a['source'] = list(filter(map x:x.startswith('http'),links))[0]
        sources = list(filter(lambda x: x.startswith('Источник'), ts))
        if sources:
            a['source'] = sources[0].split(':')[1].strip()
        dates = list(filter(lambda x: x.startswith('2'), ts))
        if dates:
            a['pub_date'] = dates[0]
        # a['t']=ts
        articles.append(a)
    return articles


#"Дата внесения";"Дата публикации";Адрес;Название;Текст;Портал;Автор;Источник;Снипет;Аннотация;PPR
# def parse_csv(page):
#     articles = re.split(r'(?<!\n)\n(?!\n)', page)  # lookback lookahead

#     fields = ['pub_date', 'save_date', 'link',
#               'title', 'text',
#               'news_portal', 'author', 'source',
#               'snippet', 'annotation', 'ppr']
#     for art in articles[1:]:
#         values = art.split(';(?!\s)')
#         if len(values) == 12:
#             db_art = Article()
#             for i in range(len(fields)):
#                 setattr(db_art, fields[i], values[i])
#             print(db_art)
        
