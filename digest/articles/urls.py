from django.conf.urls import url
from . import views

urlpatterns = [
    url('test', views.test_haml, name='haml'),
    url('new', views.new_article, name='create'),
    url('^search', views.index, name='index'),
]