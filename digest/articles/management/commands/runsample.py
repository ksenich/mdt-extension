from django.core.management.base import BaseCommand
from articles.models import Article
from articles.data.parse import get_articles

import re
import codecs

class Command(BaseCommand):
    def handle(self, **options):
        with codecs.open('samples/data.csv', mode='r', encoding='utf-8') as file:
            page = file.read()
            parse_csv(page)
        
def create_article():
    with open('/home/ubuntu/workspace/mdt/samples/samsung.html','r') as file:
        page = file.read()
        art = get_articles(page)
        for a in art:
            # print(a)
            na = Article()
            na.text = a.get('text')
            na.title = a.get('title')
            na.created_at = a.get('pub_date')
            na.source = a.get('source')
            na.link = a.get('link')
            na.save()

def parse_csv(page):
    # articles = re.split(r'(?<!\n)\n(?!\n)', page)  # lookback lookahead
    articles = re.split(r'\n(?=\d+)', page)
    out = codecs.open('splitters.txt', mode='w', encoding='utf-8')
    fields = ['id', 'save_date', 'pub_date', 'link',
              'title', 'text',
              'news_portal', 'author', 'source',
              'snippet', 'annotation', 'ppr']
    for art in articles[1:]:
        values = art.split(';')
        if len(values) == 12:
            db_art = Article()
            for i in range(1, len(fields)):
                field = fields[i]
                v = values[i]
                if field.endswith('date'):
                    v = re.sub('[^\d\s\-:.]', '', v)
                setattr(db_art, field, v or None)
            try:
                db_art.save()
            except Exception:
                print(values[0])
        # print(art)
    out.write(('\n'+'-'*80+'\n').join(articles[1:]))
    out.close()