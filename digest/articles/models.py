from django.db import models

# Create your models here.

#"Дата внесения";"Дата публикации";Адрес;Название;Текст;Портал;Автор;Источник;Снипет;Аннотация;PPR
class Article(models.Model):
    title = models.CharField(max_length=200)
    source = models.CharField(max_length=200, null=True)
    link = models.CharField(max_length=200, null=True, blank=True)
    save_date = models.DateTimeField(auto_now_add=True)
    pub_date = models.DateTimeField(null=True, blank=True)
    text = models.TextField()
    snippet = models.CharField(max_length=200, null=True)
    annotation = models.CharField(max_length=200, null=True)
    news_portal = models.CharField(max_length=200, null=True)
    author = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.title
