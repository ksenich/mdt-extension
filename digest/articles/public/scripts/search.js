var dayAgo = new Date();
dayAgo.setDate(dayAgo.getDate()-1);
document.querySelector('[name=start]').valueAsDate = dayAgo;
document.querySelector('[name=end]').valueAsDate = new Date();