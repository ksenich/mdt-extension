var docXml = new DOMParser().parseFromString(document_xml__template, 'text/xml');
var body = docXml.firstElementChild.firstElementChild;
var dm = {
  doc: docXml,
  e: function (tag, attr, childs) {
    var elem = this.doc.createElement(tag);
    for (var i in attr) {
      elem.setAttribute(i, attr[i]);
    }
    for (var i in childs) {
      elem.appendChild(childs[i]);
    }
    return elem;
  },
  t: function (text) {
    return this.doc.createTextNode(text);
  }
}
// var p1 = dm.e('w:p', {}, [
//   dm.e('w:r', {}, [
//     dm.e('w:t', {}, [
//       dm.t('Text')
//     ])
//   ])
// ]);
// var sec = body.firstElementChild;
// body.insertBefore(p1, sec);
// body.appendChild(p1);
// var document_xml = new XMLSerializer().serializeToString(docXml);

function Tag(node, attrs) {
  this.node = node;
  for (var i in attrs) {
    node.setAttribute(i, attrs[i]);
  }
}

Tag.prototype.tag = function (name, attrs, constructor) {
  var doc = this.node.ownerDocument;
  var tag = doc.createElement(name);
  this.node.appendChild(tag);
  if (constructor) {
    return new constructor(tag, attrs);
  } else {
    return new Tag(tag, attrs);
  }
}

Tag.prototype.text = function (text) {
  var doc = this.node.ownerDocument;
  var tn = doc.createTextNode(text);
  this.node.appendChild(tn);
}

Tag.prototype.attr = function (attr, value) {
  this.node.setAttribute(attr, value);
}

Run.prototype = Object.create(Tag.prototype);
function Run(node, attrs) {
  Tag.call(this, node, attrs);
}

Run.prototype.text = function (text) {
  var tag = this.tag('w:t');
  tag.text(text);
  return tag;
}

Run.prototype.highlight = function (color) {
  this.props = this.tag('w:rPr');
  this.props.tag('w:highlight', { 'w:val': color });
  this.node.insertBefore(this.props.node, this.node.firstElementChild);
}

Run.prototype._props = function () {
  if (!this.props) {
    var pPr = this.tag('w:rPr');
    this.props = pPr;
    this.node.insertBefore(pPr.node, this.node.firstElementChild);
  }
  return this.props;
}

Run.prototype.style = function (style) {
  var doc = this.node.ownerDocument;
  if (!this.styleTag) {
    this.styleTag = this._props().tag('w:rStyle', { 'w:val': style });
  } else {
    this.styleTag.node.setAttribute('w:val', style);
  }
}

Paragraph.prototype = Object.create(Tag.prototype);
function Paragraph(node, attrs, docx) {
  Tag.call(this, node, attrs);
  this.docx = docx;
}

Paragraph.prototype.run = function () {
  return this.tag('w:r', null, Run);
}

Paragraph.prototype.hyperlink = function (target) {
  var id = this.docx.link(target);
  return this.tag('w:hyperlink', { 'r:id': id }, Hyperlink);
}

Paragraph.prototype._props = function () {
  if (!this.props) {
    var pPr = this.tag('w:pPr');
    this.props = pPr;
    this.node.insertBefore(pPr.node, this.node.firstElementChild);
  }
  return this.props;
}

Paragraph.prototype.style = function (style) {
  var doc = this.node.ownerDocument;
  if (!this.styleTag) {
    this.styleTag = this._props().tag('w:pStyle', { 'w:val': style });
  } else {
    this.styleTag.node.setAttribute('w:val', style);
  }
}

Paragraph.prototype.spacing = function (before, after) {
  if (!this.spacingTag) {
    this.spacingTag = this._props().tag('w:spacing', { 'w:before': before, 'w:after': after })
  } else {
    this.spacingTag.node.setAttribute('w:before', before);
    this.spacingTag.node.setAttribute('w:after', after);
  }
}

function Document() {
  this.zip = new JSZip();
  var parser = new DOMParser();
  this.doc = parser.parseFromString(document_xml__template, 'text/xml');
  this.doc_rels = parser.parseFromString(document_xml_rels, 'text/xml');
  this.doc_rels_node = this.doc_rels.firstElementChild;
  this.body = this.doc.firstElementChild.firstElementChild;
  this.sec = this.body.firstElementChild;
  this.links = [];
}

Document.prototype.link = function (target) {
  var id = 'rId' + (parseInt(this.doc_rels_node.children.length) + 1);
  var tag = new Tag(this.doc.createElement('Relationship'), {
    Type: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink',
    Target: target,
    Id: id,
    TargetMode: 'External',
  });
  this.doc_rels_node.appendChild(tag.node);
  return id;
}

Document.prototype.paragraph = function () {
  var p = this.doc.createElement('w:p');
  this.body.insertBefore(p, this.sec);
  return new Paragraph(p, null, this);
}

Document.prototype.toBlob = function () {
  var zip = this.zip;
  zip.folder('_rels').file('.rels', _rels);
  var wordFolder = zip.folder('word');
  var serializer = new XMLSerializer();
  var document_xml_rels = serializer.serializeToString(this.doc_rels);
  wordFolder.folder('_rels').file('document.xml.rels', document_xml_rels);
  var document_xml = serializer.serializeToString(this.doc);
  this.document_xml = document_xml;
  wordFolder.file('document.xml', document_xml);
  wordFolder.file('fontTable.xml', fontTable_xml);
  wordFolder.file('styles.xml', styles_xml);
  zip.file('[Content_Types].xml', ct_xml);
  return zip.generateAsync({ type: 'blob' });
}

Hyperlink.prototype = Object.create(Tag.prototype);
function Hyperlink(node, attrs) {
  Tag.call(this, node, attrs);
}

Hyperlink.prototype.run = function () {
  var run = this.tag('w:r', null, Run);
  run.style('InternetLink');
  return run;
}