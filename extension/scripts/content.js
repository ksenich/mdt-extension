// (function () {
var DOM = {
  e: function (name, attrs, children) {
    var e = document.createElement(name);
    for (var i in attrs) {
      if (typeof (attrs[i]) == "object") {
        Object.assign(e[i], attrs[i]);
      } else {
        e[i] = attrs[i];
      }
    }
    for (var i in children) {
      if (typeof (children[i]) == 'string') {
        e.appendChild(document.createTextNode(children[i]));
      } else {
        e.appendChild(children[i]);
      }
    }
    return e;
  },
  t: function (text) {
    return document.createTextNode(text);
  }
}

function query(q, el) {
  el = el || document;
  return Array.prototype.slice.apply(el.querySelectorAll(q));
}

function createArticleList() {
  var headerList = query("h2"), a = [];
  for (var h in headerList) {
    var titleElement = headerList[h];
    var elementList = [], body = [], keywordList = [], ps = [], as = [], date = null;
    for (el = titleElement;
      el && "HR" != el.tagName;
      el = el.nextSibling) {
      elementList.push(el);
      if ("P" == el.tagName) {
        body.push(el.innerText);
        ps.push(el);
        var kws = query('span', el);
        keywordList.push.apply(keywordList, kws);
      }
      if (el.nodeType == Node.TEXT_NODE) {//text
        if (el.textContent.indexOf("http") == 0) {
          as.push({ href: el.textContent, text: el.textContent });
        }
        if (!isNaN(new Date(el.textContent))) {
          date = el.textContent;
        }
      }
      if (el.tagName == "A") {
        as.push({ text: el.innerText, href: el.href });
      }
    }
    // elementList.push(document.createElement("hr"));
    a.push({
      title: titleElement.textContent,
      titleElement: titleElement,
      list: elementList,
      body: body.join("\n"),
      keywordList: keywordList,
      links: as,
      ps: ps,
      date: date,
    });
  }
  return a;
}

function hideImgs() {
  var imgs = query("img");
  imgs.forEach((i) => {
    i.remove();
    // i.setAttribute('data-src', i.src);
    // i.removeAttribute('src')
  });
  var iframes = query("iframe");
  iframes.forEach((i) => {
    i.remove();
    // i.setAttribute('data-src', i.src);
    // i.removeAttribute('src')
  });
}

function insertKeywords(articles) {
  /**
   * if no highlights
   */
  // var searchq = new URL(document.referrer).searchParams.get('search_cl');
  // var searchre = new RegExp(searchq
  //   .replace(/\*/ig, "\\S*")
  //   .replace(/[\(\) \|]+/ig, '|')
  //   .replace(/^\||\|$|\|\|+/ig, ''), 'ig');
  articles.forEach(function (a) {
    // var ks = a.body.match(searchre);
    var kl = a.keywordList;
    var ks = a.keywordList.map((k) => k.innerText);
    var kd = DOM.e('div', null, ["keywords: "]);
    ks.forEach(function (k, i, ar) {
      var span = DOM.e('span',
        { style: { color: 'red', background: 'yellow' } },
        [DOM.t(k)]);
      kd.appendChild(span);
      if (i !== ar.length - 1) {
        kd.appendChild(DOM.t(', '));
      }
    });
    ks.sort();
    ks = ks.filter((k, i, ar) => k !== ar[i - 1]);
    var ukd = DOM.e('div', null, ['unique keywords: ']);
    ks.forEach(function (k, i, ar) {
      var span = DOM.e('span',
        { style: { color: 'red', background: 'yellow' } },
        [DOM.t(k)]);
      ukd.appendChild(span);
      if (i !== ar.length - 1) {
        ukd.appendChild(DOM.t(', '));
      }
    });
    // console.log(ks)
    document.body.insertBefore(kd, a.titleElement);
    document.body.insertBefore(ukd, a.titleElement);
  });
}

function sortArticles(articles) {
  articles.sort(function (a, b) { return a.title.localeCompare(b.title) });
  var body = document.createElement("body");
  for (var i in articles) {
    for (var j in articles[i].list)
      body.appendChild(articles[i].list[j]);
    body.appendChild(document.createElement("hr"));
  }
  document.children[0].replaceChild(body, document.body)
};

function addTools(articles) {
  for (var i in articles) {
    var toolbox = DOM.e('div', null, [
      DOM.e('button', {
        className: 'copy',
        onclick: function (a) {
          copyElements(a);
        }.bind(null, articles[i])
      }, ['copy']),
      DOM.e('button', { className: 'join' }, ['join']),
      DOM.e('button', { className: 'drop' }, ['drop']),
    ]);
    var t = articles[i].titleElement;
    t.parentNode.insertBefore(toolbox, t);
  }
}

function copyElements(article) {
  var s = getSelection();
  s.removeAllRanges();
  var r = document.createRange();
  r.setStart(article.list[0], 0);
  r.setEndAfter(article.list[article.list.length - 1]);
  s.addRange(r);
  return document.execCommand('copy');
}

function createReference(articles) {
  var ref = DOM.e('ul', { id: 'ref', style: { background: 'lightgray' } });
  for (var i in articles) {
    var a = articles[i];
    a.titleElement.id = i;
    ref.appendChild(DOM.e('li', null, [
      DOM.e('a', { href: "#" + i }, [a.title]),
    ]));
    var last = a.list[a.list.length - 1];
    document.body.insertBefore(DOM.e('a', { href: '#ref' }, ['top']), last);
  }
  document.body.insertBefore(ref, document.body.children[0]);
}

function exportDocx(articles) {
  var doc = new Document();
  for (var i in articles) {
    var article = articles[i];
    var title = doc.paragraph();
    title.style('Heading1');
    title.run().text(article.title);
    if (article.keywordList.length) {
      var firstKeyword = true;
      var par = doc.paragraph();
      for (var j in article.keywordList) {
        if (firstKeyword) {
          firstKeyword = false;
        } else {
          par.run().text(',');
        }
        var run = par.run();
        run.highlight('yellow');
        run.text(article.keywordList[j].textContent);
      }
      par.spacing(60, 120);
    }
    for (var j in article.links) {
      var link = article.links[j];
      var apar = doc.paragraph();
      var hl = apar.hyperlink(link.href).run().text(link.text);
    }
    if (article.date) {
      var par = doc.paragraph();
      par.run().text(article.date);
      par.spacing(120, 120);
    }
    for (var j in article.ps) {
      var p = article.ps[j];
      var par = doc.paragraph();
      for (var k = 0, kl = p.childNodes.length; k < kl; ++k) {
        var chunk = p.childNodes[k];
        if (chunk.textContent) {
          var run = par.run();
          if (chunk.nodeType == Node.ELEMENT_NODE && chunk.nodeName == "SPAN") {
            run.highlight('yellow');
          }
          run.text(chunk.textContent).attr('xml:space', 'preserve');
        }
      }
    }
  }
  doc.toBlob().then(downloadBlob);
}

function downloadBlob(blob) {
  var url = URL.createObjectURL(blob);
  var link = document.createElement('a');
  link.href = url;
  link.innerText = 'load';
  link.download = 'file.docx';
  link.click();
}


window.articles = [];
if (document.readyState == 'interactive' || document.readyState == 'complete') {
  hideImgs();
  window.articles = createArticleList();
} else {
  document.addEventListener("DOMContentLoaded", function (event) {
    window.articles = createArticleList();
  });
}

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    console.log('action=', request.action);
    switch (request.action) {
      case 'keywords':
        insertKeywords(window.articles);
        sendResponse({ action: "keywords" });
        break;
      case "sort":
        sortArticles(window.articles);
        sendResponse({ action: "sort" });
        break;
      case "tools":
        addTools(window.articles);
        sendResponse({ action: "tools" });
        break;
      case "reference":
        createReference(window.articles);
        sendResponse({ action: "reference" });
        break;
      case "restructure":
        restructureDocument(articles);
        sendResponse({ action: "restructure" });
        break;
      case "export":
        exportDocx(articles);
        sendResponse({ action: "export" });
    }
  });
// }())