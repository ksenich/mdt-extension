
function restructureDocument(articles) {
  var ref, arts;
  var e = DOM.e;
  var body = e('body', null, [
    ref = e('ul', { id: 'ref', style: { background: 'lightgray' } }),
    arts = e('div', { id: 'articles' }),
  ]);
  for (var i in articles) {
    var a = articles[i];
    var articleNode, copyButton;
    var article = e('div', { id: i }, [
      e('div', { className: 'tools' }, [
        copyButton = e('button', {
          className: 'copy',
        }, ['copy']),
        e('button', { className: 'join' }, ['join']),
        e('button', { className: 'drop' }, ['drop']),
      ]),
      e('div', { className: 'keywords' },
        a.keywordList.map((k) => e("span", {
          color: 'red',
          background: 'yellow',
          margin: '5px',
        }, [k]))),
      articleNode = e('div', { className: 'article' }, [
        a.titleElement,
        e('div', { className: 'links' },
          a.links.map(l => e('a', { href: l }, [l]))),
        e('div', { className: 'articleBody' }, a.ps),
      ]),
      e('a', { href: '#ref' }, ['top']),
      e('hr'),
    ]);
    copyButton.onclick = function (a) {
      var s = getSelection();
      s.removeAllRanges();
      var r = document.createRange();
      r.selectNode(a);
      s.addRange(r);
      document.execCommand('copy');
    }.bind(null, articleNode)
    ref.appendChild(e('li', null, [
      e('a', { href: "#" + i }, [a.title]),
    ]));
    arts.appendChild(article);
  }
  document.children[0].replaceChild(body, document.body);
}


// chrome.runtime.onMessage.addListener(
//   function (request, sender, sendResponse) {
//     console.log('action=', request.action);
//     switch (request.action) {
//       case "restructure":
//         restructureDocument(articles);
//         sendResponse({ action: "restructure" });
//         break;
//     }
//   });