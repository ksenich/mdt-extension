function xmlToJson(xml) {

  // Create the return object
  var obj = {};
  obj.tag = xml.nodeName;
  if (xml.nodeType == Node.ELEMENT_NODE) { // element
    // do attributes
    if (xml.attributes.length > 0) {
      obj["attributes"] = {};
      for (var j = 0; j < xml.attributes.length; j++) {
        var attribute = xml.attributes.item(j);
        obj["attributes"][attribute.nodeName] = attribute.nodeValue;
      }
    }
  } else if (xml.nodeType == Node.TEXT_NODE) { // text
    obj = xml.textContent;
  }

  obj.children = [];
  if (xml.hasChildNodes()) {
    for (var i = 0; i < xml.childNodes.length; i++) {
      var item = xml.childNodes.item(i);
      obj.children.push(xmlToJson(item));
    }
  }
  return obj;
};

function jsonToXml(obj, doc) {
  if (!doc) {
    var doc = document.implementation.createDocument(null, obj.tag);
    var root = doc.firstElementChild;
  } else {
    if (typeof obj == 'object') {
      root = doc.createElement(obj.tag);
    } else {
      root = doc.createTextElement(obj);
    }
  }
  for (var i in obj.attributes) {
    root.setAttribute(i, obj.attributes[i]);
  }
  for (var i in obj.children) {
    root.appendChild(jsonToXml(obj.children[i], doc));
  }
  return root;
}
function strToJson(str) {
  return xmlToJson(new DOMParser('text/xml').parseFromString(str));
}
var zipStructure = {
  type: 'folder',
  contents: [
    {
      type: 'folder', name: '_rels', contents: [
        { type: 'file', name: '.rels', contents: strToJson(_rels) }
      ]
    },
    {
      type: 'folder', name: 'word', contents: [
        {
          type: 'folder', name: '_rels', contents: [
            { type: 'file', name: 'document.xml.rels', contents: strToJson(document_xml_rels) }
          ]
        },
        { type: 'file' }
      ]
    }
  ]
}