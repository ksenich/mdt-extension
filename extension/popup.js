function sendMessageToContent(request, cb) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, request, cb);
  });
}

toolify('sort');
toolify('restructure');
toolify('reference');
toolify('keywords');
toolify('tools');
toolify('export');

function toolify(foo) {
  var rb = document.querySelector('#' + foo);
  rb.onclick = function reference() {
    rb.onclick = null;//prevent doubleclick
    sendMessageToContent({ action: foo }, function () {
      rb.onclick = reference;
    });
  }
}
