var Sequelize = require('sequelize');
var path = require('path');
var sequelize = new Sequelize('db', 'root', '', {
  dialect: 'sqlite',
  storage: path.join(__dirname, 'database.sqlite')
})

const User = sequelize.define('user', {
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  }
});

const Subscriber = sequelize.define('subscriber', {
  name: {
    type: Sequelize.STRING
  }
});

const MailTarget = sequelize.define('mail_target', {
  address: {
    type: Sequelize.STRING
  }
});

Subscriber.hasMany(MailTarget, { as: 'Mails' });

sequelize.sync();

var models = {
  users: User,
  subscribers: Subscriber,
  mails: MailTarget,
}

module.exports = models;