const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');

const mailer = require('./mailer');
const models = require('./model');
const rest = require('./rest');

app.use(cors());
app.use(morgan('combined'));

app.post('/mail', bodyParser.urlencoded({ extended: false, type: () => true }), (req, res) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://mediateka.com.ua');
  let html = req.body.html;
  let receiver = req.body.receiver;
  models.subscribers.findOne({
    name: req.body.receiver
  })
    .then(one => one.getMails())
    .then(mails => mails.map(m => m.address))
    .then(list => mailer.sendMail(list, html))
    .then(() => res.redirect('/'))
    .catch(error => {
      console.log(error);
      res.status(500).send(error);
    });
});

app.use('/api', rest);
app.use(express.static(path.join(__dirname, 'public'), { extensions: 'html' }));
app.listen(4000);
