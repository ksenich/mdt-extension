// create reusable transporter object using the default SMTP transport
const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    host: 'smtp.ukr.net',
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: 'idliketoplay@ukr.net',
        pass: 'RattlesnakesCome' // not sending one
    }
});

function sendMail(list, body) {
    return new Promise((resolve, reject) => {
        if (!list.length) {
            resolve();
            return;
        }
        let mailOptions = {
            from: '"MDT Mailer" <idliketoplay@ukr.net>',
            to: list.toString(),
            subject: 'digest',
            html: body
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                reject(error);
                return;
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
            resolve();
        });
    });
}

module.exports = {
    sendMail
};