var targetUrl = createTargetUrl({
  search_cl: '(фарма*|компан* NEAR/5 Дарниц*)|фармак|фармаку|фармаком|Sopharma|Софарм*|Takeda|Такед*|Ipsen|(Bayer + фарма*)|Acino|Фармастарт|Stada|Bionorica|Cegis|"Orion pharma"|"Berlin-Chemie"|Novartis|Teva|"Борщагівськ* хімік*"|"Борщаговск* химик*"|"Борщагівськ* ХТЗ"|"Борщаговск* ХТЗ"|Інтерхім*|интерхим*|"Kusus Pharm"|"Юрія фарм"|"Юрия фарм"|індар|индар|артеріум|артериум|"компан* здоров*"',
  date_from: '2017-05-30 08:00:01',
  date_to: '2017-05-31 08:00:00',
  data_zag: 1,
  page: 0,
  list: 1
});
var mailerUrl = '';
var receiver = 'test1';
var i1 = document.createElement('iframe');
i1.name = 'i1';
document.body.appendChild(i1);
var w2 = window.open(targetUrl, 'i1');
var login = 'staff13-test';
var passw = 'v4671';
var ele, doc1;

i1.onload = function () {
  doc1 = i1.contentDocument;
  ele = doc1.getElementsByTagName('center')[0];
  if (ele) {
    console.log('center')
    doc1.getElementsByTagName('input')[0].value = login;
    doc1.getElementsByTagName('input')[1].value = passw;
    doc1.getElementById('search-submit').click();
  } else {
    //console.log('!center');
    form = doc1.querySelector('[action="/export_data.php"]');
    if (form) {
      //console.log('form');
      form.target = '_self';
      ele = doc1.getElementsByName('in_html')[0];
      ele.click();
    } else {
      //console.log('!form');
      if (doc1.getElementsByName('search_cl').length) {
        //console.log('search_cl');
        i1.contentWindow.location.assign(targetUrl);
      } else {
        //console.log('exported');
        cutTexts(doc1, i1);
      }
    }
  }
}

function cutTexts(doc1, iframe) {
  var h = doc1.getElementsByTagName('h2');
  var textLimit = 300;
  var p0, p1, pCount, n, hr;
  var d = doc1.implementation.createHTMLDocument();

  for (var i = 0; i < h.length; i++) {
    n = h[i].nextElementSibling;
    pCount = 0;
    if (!n) continue;
    while (n = n.nextElementSibling) {
      if (n.tagName !== 'HR' && pCount < 2) {
        if (n.tagName == 'P' && n.textContent.length > 1) {
          pCount++;
          (pCount == 1) ? p0 = n : p1 = n;
        }
      } else {
        var tl = trimText(p0, p1, textLimit, h[i]);
        d.body.appendChild(tl);

        hr = d.createElement("hr"); d.body.appendChild(hr);
        break;
      }
    }
  }

  function trimText(p0, p1, max, h) {
    var r = document.createRange();
    r.setStartBefore(h);
    var ellipsize = false;
    try {
      if (p0.textContent.length > max) {
        // console.log('ei',endIndex(p0, max));
        var f = getRangeParams(p0, endIndex(p0, max));
        r.setEnd(f.node, f.index);
        ellipsize = true;
      } else {
        var diff = max - p0.textContent.length;
        var f = getRangeParams(p1, endIndex(p1, diff));
        r.setEnd(f.node, f.index);
        if (diff > 0) {
          ellipsize = true;
        }
      }
    } catch (e) {
      console.log('range error', p0, p1);
    }
    var fragment = r.cloneContents();
    if (ellipsize) {
      fragment.lastChild.textContent += '..';
    }
    return fragment;
  }

  function endIndex(p, max) {			//cant just use str.indexOf('.') ? I find approach with indexOf() less readable. But we certainly can.
    var t = p.textContent.split('.');
    var acc = 0, accl = [];
    for (var i = 0; i < t.length; ++i) {
      if (acc < max) {
        // accl.push(t[i]);
        acc += t[i].length + 1;
      } else {
        break;
      }
    }
    return acc;
  }

  function getRangeParams(element, index) {
    var acc = 0;
    if (element.nodeType === Node.TEXT_NODE) {
      return {
        node: element,
        index: Math.min(index, element.textContent.length)
      }
    } else {
      for (var i = 0, l = element.childNodes.length; i < l; ++i) {
        var node = element.childNodes[i];
        if (acc + node.textContent.length > index) {
          return getRangeParams(node, index - acc);
        } else {
          acc += node.textContent.length;
        }
      }
    }
    return {
      node: element,
      index: element.childNodes.length,
    };
  }
  var images = d.getElementsByTagName('IMG');
  while (images.length > 0) {
    images[0].remove();
  }
  var serializer = new XMLSerializer();
  var result = serializer.serializeToString(d);
  var textFile = null,
    makeTextFile = function (text) {
      var data = new Blob([text], { type: 'text/plain' });

      // If we are replacing a previously generated file we need to
      // manually revoke the object URL to avoid memory leaks.
      if (textFile !== null) {
        window.URL.revokeObjectURL(textFile);
      }
      textFile = window.URL.createObjectURL(data);
      return textFile;
    };

  iframe.contentDocument.body.innerHTML = result;

  var bodyParams = jsonToUrlencoded({
    receiver: receiver,
    html: result
  });
  fetch(mailerUrl, {
    method: 'POST',
    headers: new Headers({ 'content-type': 'application/x-www-form-urlencoded' }),
    body: bodyParams
  }).then(() => {
    alert('mail sent');
  }).catch(e => {
    alert('mail failed');
  });
	/*iframe.contentDocument.replaceChild(
		iframe.contentDocument.importNode(dd.documentElement, true),
		iframe.contentDocument.documentElement
	);*/

  function sortStuff() {
    function f(a) {
      var b = iframe.contentDocument.querySelectorAll("h2"),
        c = Array.prototype.slice.apply(b);
      for (var d in c) {
        for (var e = c[d], f = [], g = e; g && "HR" != g.tagName;) f.push(g), g = g.nextSibling;
        f.push(iframe.contentDocument.createElement("hr")), a.push({
          title: e.textContent,
          titleElement: e,
          list: f
        })
      }
    }
    var a = [];
    f(a);
    iframe.contentDocument.querySelector("body");
    a.sort(function (a, b) {
      return a.title.localeCompare(b.title)
    });
    var c = iframe.contentDocument.createElement("body");
    for (var d in a)
      for (var e in a[d].list) c.appendChild(a[d].list[e]);
    iframe.contentDocument.children[0].replaceChild(c, iframe.contentDocument.body);
    result = iframe.contentDocument.body.innerHTML;
  }
  sortStuff();

	/*var link = document.createElement('a');
	link.href = makeTextFile(result);
	link.download = 'short.html'
	link.click();*/
}

function createTargetUrl(query) {
  // search_cl, date_from, date_to, data_zag, page, list
  return 'http://mediateka.com.ua/?' + jsonToUrlencoded(query);
}

function jsonToUrlencoded(query) {
  var list = [];
  for (var i in query) {
    list.push(i + '=' + encodeURIComponent(query[i]));
  }
  return list.join('&');
}