var express = require('express');
const bodyParser = require('body-parser');
var app = express.Router();
const models = require('./model');

app.get('/:model/:id?/:field?/:field_id?', function (req, res) {
  var queryResult;
  console.log(req.params);
  var params = req.params;
  var model = models[params.model];
  var associations = model.associations;
  if (req.params.id) {
    queryResult = model
      .findAll({ where: { id: params.id } });
  } else {
    queryResult = model
      .findAll();
  }
  queryResult = queryResult
    .then(function (results) {
      if (!results) {
        return [];
      }
      if (params.field) {
        var get = model.associations[capitalize(params.field)].accessors.get;
        var options;
        if (params.field_id) {
          options = { where: { id: +params.field_id } };
        }
        return results[get](options);
      } else {
        return results;
      }
    });
  if (!params.field)
    for (let ass in associations) {
      var a = associations[ass];
      console.log('ass', ass);
      queryResult = queryResult
        .then(value => {
          if (value.map) {
            return Promise.all(value.map(v => fetchAssociations(v, ass, a.accessors.get)));
          } else return fetchAssociations(value, ass, a.accessors.get);
        })
    }
  queryResult.then(value => {
    res.json(toValue(value));
  })
});

app.post('/:model', bodyParser.json(), function (req, res) {
  models[req.params.model]
    .create(req.body)
    .then(e => res.json(e.toJSON()))
    .catch(e => console.log(e));
});

function toValue(results) {
  if (results.map) {
    return results.map(e => toValue(e));
  } else if (results.toJSON) {
    return results.toJSON();
  } else {
    return results;
  }
}

function capitalize(str) {
  x = str.replace(/^\w/, match => match.toUpperCase());
  return x;
}

function decapitalize(str) {
  x = str.replace(/^\w/, match => match.toLowerCase());
  return x;
}

function fetchAssociations(value, ass, get) {
  return value[get]()
    .then(v => {
      value = value.toJSON()
      value[decapitalize(ass)] = toValue(v);
      return value;
    });
}
module.exports = app;